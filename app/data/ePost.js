import { Images } from "@config";

const EPostListData = [
  {
    id: 1,
    description: "Sandalias planaforma, cómodas con detalles en murano, justas para quienes aman estar cómodas y frescas.",
    title: "14519-3281",
    image: Images.productView,
    costPrice: "$39.000",
    salePrice: "$29.000",
    isFavorite: false,
    salePercent: "-10%",
    price: 29,
  },
  {
    id: 2,
    description: "Sandalias planaforma, cómodas con detalles en murano, justas para quienes aman estar cómodas y frescas.",
    title: "14519-3281",
    image: Images.productView,
    costPrice: "",
    salePrice: "$49.000",
    isFavorite: true,
    salePercent: "",
    price: 49,
    isBestMatch: true,
  },
  {
    id: 3,
    description: "Sandalias planaforma, cómodas con detalles en murano, justas para quienes aman estar cómodas y frescas.",
    title: "14519-3281",
    image: Images.productView,
    costPrice: "$40.000",
    salePrice: "$50.000",
    isFavorite: false,
    salePercent: "-10%",
    price: 50,
    isBestMatch: true,
  },
  {
    id: 4,
    description: "Botines con accesorios en cadenas ideal para llevar tu look a otro plano.",
    title: "14519-3281",
    image: Images.productView,
    costPrice: "$79.000",
    salePrice: "$69.000",
    isFavorite: false,
    salePercent: "-10%",
    price: 69,
  },
  {
    id: 5,
    description: "Botines con accesorios en cadenas ideal para llevar tu look a otro plano.",
    title: "14519-3281",
    image: Images.productView,
    costPrice: "$49.000",
    salePrice: "$39.000",
    isFavorite: false,
    salePercent: "-10%",
    price: 39,
    isBestMatch: true,
  },
  {
    id: 6,
    description: "Tenis básicos y cómodos tipo media acompañados de un diseño clásico",
    title: "14519-3281",
    image: Images.productView,
    costPrice: "$39.00",
    salePrice: "$29.00",
    isFavorite: false,
    salePercent: "-10%",
    price: 29,
  },
];

const ESortOption = [
  {
    value: "all",
    text: "all",
  },
  {
    value: "best_match",
    text: "best_match",
  },
  {
    value: "price_low_to_high",
    text: "price_low_to_high",
  },
  {
    value: "price_high_to_low",
    text: "price_high_to_low",
  },
];

export { EPostListData, ESortOption };
