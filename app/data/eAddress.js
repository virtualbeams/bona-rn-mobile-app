export const EAddressData = [
  {
    id: 1,
    name: "Dirección Casa",
    address:
      "Calle 72 # 50 Medellín",
    phone: "3206791010",
    city: 'Medellín'
  },
  {
    id: 2,
    name: "Dirección Oficina",
    address:
      "Calle 10 # 50 sur",
    phone: "3206791010",
    city: 'Medellín'
  }
];
