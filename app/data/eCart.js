import { Images } from "@config";

export const ProductsData = [
  {
    id: 1,
    description: "Color Azul - Talla 35",
    secondDescription: "",
    title: "14525-TODE",
    image: Images.productView,
    salePrice: "$78.000",
    isFavorite: false,
    price: 78000,
  },
  {
    id: 2,
    description: "Color Azul - Talla 35",
    secondDescription: "",
    title: "14525-TODE",
    image: Images.productView,
    salePrice: "$59,000",
    isFavorite: true,
    price: 59000,
  }
];
