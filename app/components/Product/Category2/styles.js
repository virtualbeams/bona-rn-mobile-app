import { StyleSheet } from "react-native";

export default StyleSheet.create({
  container: {
    flex: 1,
    borderWidth: 2,
    marginHorizontal: 3,
    borderRadius: 5,
    borderColor: '#f2f2f2'
  },
  imageBackground: {
    height: 160,
    width: "100%",
    justifyContent: "flex-end",
  },
  content: {
    width: "100%",
    padding: 6,
  },
  viewText: {
    borderRadius: 8,
    paddingHorizontal: 6,
    paddingVertical: 4,
  },
});
