import { Images } from "@config";

export const ECategories = [
  {
    title: "Baletas",
    icon: "gift",
    imageIcon: Images.catBaletas,
  },
  {
    title: "Botines",
    icon: "futbol",
    imageIcon: Images.catBotin,
  },
  {
    title: "Mocasines",
    icon: "mobile-alt",
    imageIcon: Images.catMocasin,
  },
  {
    title: "Sandalias",
    icon: "laptop",
    imageIcon: Images.catSandals,
  },
  {
    title: "Tacones",
    icon: "headphones",
    imageIcon: Images.catHighHeels,
  },
  {
    title: "Tenis",
    icon: "hand-peace",
    imageIcon: Images.catTennis,
  },
  {
    title: "Niña",
    icon: "hand-peace",
    imageIcon: Images.catNina,
  },
];

export const EFeaturedShop = [
  {
    image: Images.branchApple,
    title: "Apple",
    rating: 4.5,
    totalRating: "(29)",
    description: "300 products",
  },
  {
    image: Images.branchSamsung,
    title: "Samsung",
    rating: 4.5,
    totalRating: "(10)",
    description: "300 products",
  },
];

export const EPopulars = [
  {
    id: 1,
    description: "Tenis básicos y cómodos tipo media acompañados de un diseño clásico",
    title: "14519-3281",
    image: Images.productView,
    costPrice: "$39.000",
    salePrice: "$29.000",
    isFavorite: false,
    price: 29000,
  },
  {
    id: 2,
    description: "Tenis básicos y cómodos tipo media acompañados de un diseño clásico",
    title: "14519-3281",
    image: Images.productView,
    costPrice: "$59.000",
    salePrice: "$39.000",
    isFavorite: true,
    price: 39000,
  },
  {
    id: 3,
    description: "Tenis básicos y cómodos tipo media acompañados de un diseño clásico",
    title: "14519-3281",
    image: Images.productView,
    costPrice: "$39.000",
    salePrice: "$19.000",
    isFavorite: false,
    price: 19000,
  },
  {
    id: 4,
    description: "Tenis básicos y cómodos tipo media acompañados de un diseño clásico",
    title: "14519-3281",
    image: Images.productView,
    costPrice: "$59.000",
    salePrice: "$39.000",
    isFavorite: true,
    price: 39000,
  },

  {
    id: 5,
    description: "Tenis básicos y cómodos tipo media acompañados de un diseño clásico",
    title: "14519-3281",
    image: Images.productView,
    costPrice: "$59.000",
    salePrice: "$39.000",
    isFavorite: true,
    price: 39000,
  },

  {
    id: 6,
    description: "Tenis básicos y cómodos tipo media acompañados de un diseño clásico",
    title: "14519-3281",
    image: Images.productView,
    costPrice: "",
    salePrice: "$49.000",
    isFavorite: true,
    price: 49000,
  },
];

export const EYourStores = [
  {
    value: "address1",
    icon: "sort-amount-up",
    text: "Calle 77 # 82 Medellín",
    image: Images.co,
  },
  {
    value: "address2",
    icon: "sort-amount-up",
    text: "Calle 10sur # 80 Medellín",
    image: Images.co,
  },
];
