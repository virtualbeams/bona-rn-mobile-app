export const EFilterColors = [
  {
    name: "Gray",
    color: "#D8D8D8",
  },
  {
    name: "Red-Pink",
    color: "#FF2D55",
  },
  {
    name: "Red",
    color: "#F90030",
  },
  {
    name: "Pink",
    color: "#FF5E80",
  },
  {
    name: "Green-Blue",
    color: "#4A90A4",
  },
  {
    name: "Black",
    color: "#212121",
  },
];

export const EFilterCategories = [
  { id: "1", name: "Baletas", checked: true },
  { id: "2", name: "Botines" },
  { id: "3", name: "Mocasines" },
  { id: "4", name: "Sandalias" },
  { id: "5", name: "Tacones" },
  { id: "6", name: "Tenis" },
  { id: "7", name: "Niñas" },
];

export const EFilterSizes = [
  {
    id: "35",
    name: "35",
  },
  {
    id: "36",
    name: "36",
  },
  {
    id: "37",
    name: "37",
  },
  {
    id: "38",
    name: "38",
  },
  {
    id: "39",
    name: "39",
  },
  {
    id: "40",
    name: "40",
  }
];
