import {
  CategoryIconSoft,
  HeaderAnimated,
  Icon,
  Image,
  ModalFilter,
  ProductGrid1,
  SafeAreaView,
  ShopCard1,
  CardBanner,
  Text,
  TextInput,
  HeaderLargeTitle,
  HeaderLargeTitleStore,
  HeaderLargeTitleBadge,
  ActionButton,
} from "@components";
import { BaseColor, BaseStyle, useTheme } from "@config";
import { ECategories, EFeaturedShop, EPopulars, EYourStores } from "@data";
import { getWidthDevice } from "@utils";
import React, { useEffect, useRef, useState } from "react";
import ContentLoader, { Rect } from "react-content-loader/native";
import { useTranslation } from "react-i18next";
import { Animated, ScrollView, TouchableOpacity, View } from "react-native";
import styles from "./styles";

const TEMP_BANNERS = [
  'https://cdn.shopify.com/s/files/1/0494/5205/6744/files/banner-02ok.jpg?v=1620771178',
  'https://cdn.shopify.com/s/files/1/0494/5205/6744/files/BANNER-ULTIMAS-TALLAS.jpg?v=1617108368',
  'https://cdn.shopify.com/s/files/1/0494/5205/6744/files/banner-03ok.jpg?v=1620771011',
  'https://cdn.shopify.com/s/files/1/0494/5205/6744/files/banner-01ok.jpg?v=1620771147'
];

const HeaderLine = ({ style = {}, title = "", onPress = () => {} }) => {
  const { t } = useTranslation();

  return (
    <View
      style={[
        {
          flexDirection: "row",
          alignItems: "center",
        },
        style,
      ]}
    >
      <Text title3 style={{ flex: 1 }}>
        {title}
      </Text>
      {
      /*<TouchableOpacity onPress={onPress}>
        <Text body2 accentColor>
          {t("see_all")}
        </Text>
      </TouchableOpacity>
      */
      }
    </View>
  );
};

const Home = (props) => {
  const { navigation } = props;
  const { t } = useTranslation();
  const { colors } = useTheme();
  const [modalVisible, setModalVisible] = useState(false);
  const [search, setSearch] = useState("");
  const [loading, setLoading] = useState(true);
  const [stores, setStores] = useState(EYourStores);
  const [storeChoosed, setStoreChoosed] = useState(EYourStores[0]);
  const scrollY = useRef(new Animated.Value(0)).current;

  const onScroll = Animated.event(
    [{ nativeEvent: { contentOffset: { y: scrollY } } }],
    {
      useNativeDriver: false,
    }
  );

  const onChangeStore = () => {
    let storeChoosed = {};
    for (const store of stores) {
      if (store.checked) {
        storeChoosed = store;
        break;
      }
    }
    setStoreChoosed(storeChoosed);
    setModalVisible(false);
  };

  const onSelectStore = (store) => {
    const stores = EYourStores.map((item) => {
      return {
        ...item,
        checked: item.value == store.value,
      };
    });
    setStores(stores);
  };

  useEffect(() => {
    setTimeout(() => {
      setLoading(false);
    }, Math.floor(Math.random() * 1000) + 1000);
  }, []);

  const goCategory = (item = {}) => {
    navigation.navigate("ECategory", { item: item });
  };

  const goShop = (item) => {
    navigation.navigate("EProductStoreProfile", { item: item });
  };

  const goProductDetail = (item) => {
    navigation.navigate("EProductDetail", { item: item });
  };

  const goSearch = () => {
    navigation.navigate("ESearchHistory");
  };

  const goProducts = () => {
    navigation.navigate("EProduct");
  };

  const renderPlaceholder = () => {
    return (
      <View style={styles.paddingSrollView}>
        <ContentLoader
          speed={0.5}
          width={"100%"}
          height={"100%"}
          backgroundColor="#f3f3f3"
          foregroundColor={BaseColor.dividerColor}
        >
          <Rect x="0" y="0" rx="8" ry="8" width="40%" height="30" />
          <Rect x="0" y="40" rx="8" ry="8" width="80%" height="20" />
          <Rect x="0" y="80" rx="8" ry="8" width="100%" height={250} />

          <Rect x="0" y={350} rx="8" ry="8" width="110" height={80} />
          <Rect x="120" y={360} rx="8" ry="8" width="30%" height={10} />
          <Rect x="120" y={380} rx="8" ry="8" width="60%" height={15} />
          <Rect x="120" y={410} rx="8" ry="8" width="40%" height={10} />

          <Rect x="0" y={440} rx="8" ry="8" width="110" height={80} />
          <Rect x="120" y={450} rx="8" ry="8" width="30%" height={10} />
          <Rect x="120" y={470} rx="8" ry="8" width="60%" height={15} />
          <Rect x="120" y={495} rx="8" ry="8" width="40%" height={10} />

          <Rect x="0" y={530} rx="8" ry="8" width="110" height={80} />
          <Rect x="120" y={540} rx="8" ry="8" width="30%" height={10} />
          <Rect x="120" y={560} rx="8" ry="8" width="60%" height={15} />
          <Rect x="120" y={585} rx="8" ry="8" width="40%" height={10} />

          <Rect x="0" y={630} rx="8" ry="8" width="50%" height={160} />
          <Rect x="53%" y={630} rx="8" ry="8" width="50%" height={160} />
        </ContentLoader>
      </View>
    );
  };

  const renderContent = () => {
    return (
      <View style={{ flex: 1 }}>
        <HeaderAnimated
          scrollY={scrollY}
          componentLeft={<HeaderLargeTitleStore />}
          /*componentRight={
            <HeaderLargeTitleBadge
              onPress={() => navigation.navigate("ENotification")}
            />
          }*/
          componentBottom={
            <TouchableOpacity onPress={goSearch}>
              <TextInput
                autoCorrect={false}
                placeholder={t("enter_keywords")}
                value={search}
                editable={false}
                pointerEvents="none"
              />
            </TouchableOpacity>
          }
        />

        <Animated.ScrollView
          contentContainerStyle={[styles.paddingSrollView]}
          onScroll={onScroll}
        >

        {/*Not use FlatList in ScrollView */}
        <ScrollView
            contentContainerStyle={styles.paddingFlatList}
            horizontal={true}
            showsHorizontalScrollIndicator={false}
            showsVerticalScrollIndicator={false}
            style={{ marginBottom: 20 }}
          >
            {TEMP_BANNERS.map((item, index) => (
              <View style={{width: getWidthDevice() - 100, marginRight: 20}}>
                <CardBanner 
                  style={{ width: "100%", height: 100 }}
                  image={{uri: item}}
                  styleContent={{
                    position: "absolute",
                    bottom: 0,
                    padding: 10,
                  }}
                  onPress={() => {}}
                />
              </View>
            ))}
          </ScrollView>

          <HeaderLine title={t("categories")} onPress={goCategory} />
          {/*
          <Text subhead grayColor style={{ marginTop: 4 }}>
            {t("e_description_featured_shop")}
          </Text>
          */}
          {/*Not use FlatList in ScrollView */}
          <ScrollView
            contentContainerStyle={styles.paddingFlatList}
            horizontal={true}
            showsHorizontalScrollIndicator={false}
            showsVerticalScrollIndicator={false}
          >
            {ECategories.map((item, index) => (
              <CategoryIconSoft
                key={index.toString()}
                title={item.title}
                icon={item.icon}
                imageIcon={item.imageIcon}
                style={{
                  marginRight: index != ECategories.length - 1 ? 20 : 0,
                }}
                onPress={() => goProducts(item)}
              />
            ))}
          </ScrollView>

          {/* 
          <Text title3 style={{ flex: 1, paddingTop: 20 }}>
            {t("e_featured_shop")}
          </Text>

          <Text subhead grayColor style={{ marginTop: 4 }}>
            {t("e_description_featured_shop")}
          </Text>
          */}

          <HeaderLine
            style={{ paddingTop: 20 }}
            title={t("popular")}
            onPress={goProducts}
          />

          {/*Not use FlatList in ScrollView */}
          <View style={{ flexDirection: "row", flexWrap: "wrap" }}>
            {EPopulars.map((item, index) => (
              <View key={index.toString()} style={{ width: "50%" }}>
                <ProductGrid1
                  style={{
                    width: "100%",
                    paddingRight: index % 2 == 0 ? 10 : 0,
                    paddingLeft: index % 2 != 0 ? 10 : 0,
                  }}
                  description={item.description}
                  title={item.title}
                  image={item.image}
                  costPrice={item.costPrice}
                  salePrice={item.salePrice}
                  isFavorite={item.isFavorite}
                  onPress={() => goProductDetail(item)}
                />
              </View>
            ))}
          </View>
        </Animated.ScrollView>
        <ModalFilter
          options={stores}
          isVisible={modalVisible}
          onSwipeComplete={() => {
            setModalVisible(false);
          }}
          onApply={onChangeStore}
          onSelectFilter={onSelectStore}
        />
      </View>
    );
  };

  return (
    <View style={{ flex: 1 }}>
      <SafeAreaView
        style={BaseStyle.safeAreaView}
        forceInset={{ top: "always" }}
      >
        {loading ? renderPlaceholder() : renderContent()}
        {/*<ActionButton /> */}
      </SafeAreaView>
    </View>
  );
};

export default Home;
